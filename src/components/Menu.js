import React, { useState } from "react";

const Menu = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <nav className="menu">
      <button className="menu__button" onClick={toggleMenu}>
        Menu
      </button>
      <ul className={`menu__list ${isOpen ? "menu__list--open" : ""}`}>
        <li className="menu__item">
          <a href="/" className="menu__link">
            Home
          </a>
        </li>
        <li className="menu__item">
          <a href="/" className="menu__link">
            About
          </a>
        </li>
        <li className="menu__item">
          <a href="/" className="menu__link">
            Contact
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default Menu;
